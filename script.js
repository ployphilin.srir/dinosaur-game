let character = document.querySelector('.character');
let block = document.querySelector('.block');
let counter = 0;
let highScore = 0;
let score = 0;

function jump() {
  character.classList.add('animation')
  setTimeout(() => { character.classList.remove('animation') }, 300);
}

window.addEventListener("keydown", function (e) {
  let characterBottom = parseInt(window.getComputedStyle(character).getPropertyValue("bottom"));
  if (e.code === "Space" && characterBottom === -150) {
    jump();
  }
});

let isCollide = setInterval(() => {
  let characterTop = parseInt(window.getComputedStyle(character).getPropertyValue("top"));
  let blockLeft = parseInt(window.getComputedStyle(block).getPropertyValue("left"));
  if (blockLeft < 70 && blockLeft > 50 && characterTop >= 130) {
    block.style.animation = 'none';
    alert("Game Over!");
    counter = 0;
    block.style.animation = 'block 1s infinite linear';
    if (highScore < score) {
      highScore = score;
    }
    document.querySelector('.highScore').innerHTML = `HI ${highScore}`;
    score = 0;
  } else {
    counter++;
    score = convertNumber(Math.floor(counter / 10).toString())
    document.querySelector('.score').innerHTML = score;
  }
}, 10);

function convertNumber(num) {
  for (let i = num.length; num.length < 5; i++) {
    num = "0" + num;
  }
  return num
}